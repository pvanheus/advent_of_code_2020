# Advent Of Code 2020

## Code for Advent of Code 2020 (in Rust)

The `data/` folder contains test data. If run without data input parameters, the binary compiled from this project
will run the tests using that test input data. Using `.gitignore`, files with names starting with `real_`
are ignored by `git` and not committed to the repository. This is to not give away absolutely everything.2020

Self-critique: This does not *lint clean* with `cargo clippy` due to the use of `Vec<&[u8]>`. I still need to learn
more about borrowing etc in Rust.

This code is licensed under GPLv3 as described in [COPYING].
