// Copyright (C) 2020  Peter van Heusden <pvh@sanbi.ac.za>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate clap;

use std::fs::File;
use std::io::{BufRead, BufReader, Read};

use clap::{App, Arg};

use advent_of_code_2020::aoc::*;
use std::process::exit;

fn safe_open(filename: &str) -> BufReader<File> {
    match File::open(filename) {
        Ok(file) => BufReader::new(file),
        Err(e) => {
            eprintln!("Failed to open {}: {}", filename, e);
            exit(1)
        }
    }
}

fn parse_file_as_lines(filename: &str) -> Vec<String> {
    let file = safe_open(filename);
    file.lines()
        .map(|l| match l {
            Ok(line) => line,
            Err(e) => panic!("Failed to read line from file {}: {}", filename, e),
        })
        .collect()
}

fn parse_file_as_integers(filename: &str) -> Vec<i64> {
    parse_file_as_lines(filename)
        .iter()
        .map(|line: &String| match line.parse::<i64>() {
            Ok(value) => value,
            Err(e) => panic!("Failed to parse {} as an integer: {}", line, e),
        })
        .collect()
}

fn run_default_test_file_int_result(result: i64, expected: i64) {
    eprintln!("No filename provided, running default test set");
    assert_eq!(result, expected);
    println!("passed")
}

fn main() {
    let matches = App::new("Advent of Code 2020")
        .version("0.1")
        .arg(Arg::with_name("TESTNUM").required(true))
        .arg(Arg::with_name("TESTARGS").multiple(true))
        .get_matches();
    let test_num = match matches.value_of("TESTNUM") {
        Some(num_str) => num_str,
        _ => panic!("No test number provided"),
    };

    match test_num {
        "1.1" => {
            match matches.value_of("TESTARGS") {
                Some(filename) => match aoc_day1_1(parse_file_as_integers(filename)) {
                    Some(number) => println!("{}", number),
                    None => eprintln!("Got no answer for test {}", test_num),
                },
                None => {
                    let filename = "data/aoc1/test_numbers1.txt";
                    run_default_test_file_int_result(
                        aoc_day1_1(parse_file_as_integers(filename)).unwrap(),
                        514_579,
                    )
                }
            }
            // let numbers = vec![1721, 979, 366, 299, 675, 1456];
            // println!("{:?}", aoc_day1(numbers))
        }
        "1.2" => match matches.value_of("TESTARGS") {
            Some(filename) => match aoc_day1_2(parse_file_as_integers(filename)) {
                Some(number) => println!("{}", number),
                None => eprintln!("Got no answer for test {}", test_num),
            },
            None => {
                let filename = "data/aoc1/test_numbers1.txt";
                run_default_test_file_int_result(
                    aoc_day1_2(parse_file_as_integers(filename)).unwrap(),
                    241_861_950,
                );
            }
        },
        "2.1" => match matches.value_of("TESTARGS") {
            Some(filename) => {
                let num_matching_passwords =
                    aoc_day2_1(parse_file_as_lines(filename).iter().map(|s| &**s).collect());
                println!("{}", num_matching_passwords)
            }
            None => {
                let filename = "data/aoc2/test_passwords1.txt";
                run_default_test_file_int_result(
                    aoc_day2_1(parse_file_as_lines(filename).iter().map(|s| &**s).collect()) as i64,
                    2,
                )
            }
        },
        "2.2" => match matches.value_of("TESTARGS") {
            Some(filename) => {
                let num_matching_passwords =
                    aoc_day2_2(parse_file_as_lines(filename).iter().map(|s| &**s).collect());
                println!("{}", num_matching_passwords)
            }
            None => {
                let filename = "data/aoc2/test_passwords1.txt";
                run_default_test_file_int_result(
                    aoc_day2_2(parse_file_as_lines(filename).iter().map(|s| &**s).collect()) as i64,
                    1,
                )
            }
        },
        "3.1" | "3.2" => {
            let mut default_test = false;
            let filename = match matches.value_of("TESTARGS") {
                Some(value) => value,
                None => {
                    let filename = "data/aoc3/test_map.txt";
                    default_test = true;
                    eprintln!(
                        "No filename provided, running test {} on default test file",
                        test_num
                    );
                    filename
                }
            };
            let mut data: Vec<u8> = Vec::new();
            let mut reader = safe_open(filename);
            match reader.read_to_end(&mut data) {
                Ok(_) => {}
                Err(e) => panic!("Failed to read frmm file {}: {}", filename, e),
            };
            let result = if test_num == "3.1" {
                aoc_day3_1(&data[..])
            } else {
                aoc_day3_2(&data[..])
            };
            if default_test {
                if test_num == "3.1" {
                    assert_eq!(result, 7)
                } else {
                    assert_eq!(result, 336)
                }
                println!("passed");
            } else {
                println!("{}", result)
            }
        }
        _ => eprintln!("Unknown test {}", test_num),
    }
}
