// Copyright (C) 2020  Peter van Heusden <pvh@sanbi.ac.za>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod aoc {

    pub fn aoc_day1_1(numbers: Vec<i64>) -> Option<i64> {
        let target = 2020;
        for i in 0..(numbers.len() - 1) {
            for j in 1..(numbers.len() - 1) {
                if (numbers[i] + numbers[j]) == target {
                    return Some(numbers[i] * numbers[j]);
                }
            }
        }
        None
    }

    pub fn aoc_day1_2(numbers: Vec<i64>) -> Option<i64> {
        let target = 2020;
        for i in 0..(numbers.len() - 1) {
            for j in 1..(numbers.len() - 1) {
                for k in 2..(numbers.len() - 1) {
                    if (numbers[i] + numbers[j] + numbers[k]) == target {
                        return Some(numbers[i] * numbers[j] * numbers[k]);
                    }
                }
            }
        }
        None
    }

    pub fn aoc_day2_1(passwords: Vec<&str>) -> usize {
        let valid_passwords = passwords.iter().filter(|password| {
            let parts: Vec<&str> = password.split(':').collect();
            let rules = parts[0];
            let password = parts[1].trim();
            let rules_parts: Vec<&str> = rules.split(' ').collect();
            let min_max_parts: Vec<&str> = rules_parts[0].split('-').collect();
            let letter = rules_parts[1];
            let min = min_max_parts[0].parse::<i64>().unwrap();
            let max = min_max_parts[1].parse::<i64>().unwrap();
            let letter_count = password.matches(letter).count() as i64;
            letter_count <= max && letter_count >= min
        });
        valid_passwords.count()
    }

    pub fn aoc_day2_2(passwords: Vec<&str>) -> usize {
        let valid_passwords = passwords.iter().filter(|password| {
            let parts: Vec<&str> = password.split(':').collect();
            let rules = parts[0];
            let password = parts[1].trim().as_bytes();
            let rules_parts: Vec<&str> = rules.split(' ').collect();
            let pos_parts: Vec<&str> = rules_parts[0].split('-').collect();
            let letter = rules_parts[1].as_bytes()[0];
            let pos1 = pos_parts[0].parse::<usize>().unwrap();
            let pos2 = pos_parts[1].parse::<usize>().unwrap();
            // my kludge to do XOR - letter should be in one of the but not in both
            ((password[pos1 - 1] == letter) || (password[pos2 - 1] == letter))
                && !((password[pos1 - 1] == letter) && (password[pos2 - 1] == letter))
        });
        valid_passwords.count()
    }

    pub fn aoc_day3_general_grid(map_block: &[u8], x_move: usize, y_move: usize) -> i64 {
        let map_lines: Vec<&[u8]> = map_block
            .split(|&c| c == b'\n')
            .filter(|&l| l.len() > 0)
            .collect();
        let map_width = map_lines[0].len();
        let map_height = map_lines.len();
        let mut trees_seen = 0;
        let mut current_line = 0 as usize;
        let mut current_column = 0 as usize;
        while current_line < map_height {
            if current_column > map_width {
                current_column -= map_width;
            }
            eprintln!(
                "grid: {} {} {} {} {}",
                map_height,
                map_width,
                current_column,
                current_line,
                map_lines[current_line][current_column]
            );
            if map_lines[current_line][current_column] == b'#' {
                trees_seen += 1;
            }
            current_column += x_move;
            current_line += y_move;
        }
        trees_seen
    }

    pub fn aoc_day3_general_iter(map_block: &[u8], x_move: usize, y_move: usize) -> i64 {
        let map_width = map_block.iter().position(|&c| c == b'\n').unwrap();
        let mut current_line = 0 as usize;
        let mut trees_seen = 0;
        let mut next_line = 0 as usize;
        let mut next_column = 0 as usize;
        for line in map_block
            .split(|&c| c == b'\n')
            .filter(|&l| l.len() == map_width)
        {
            if current_line == next_line {
                if next_column >= map_width {
                    next_column -= map_width
                }
                if line[next_column] == b'#' {
                    trees_seen += 1;
                }
                next_line += y_move;
                next_column += x_move;
            }
            current_line += 1;
        }
        trees_seen
    }

    pub fn aoc_day3_1(map_block: &[u8]) -> i64 {
        let x_move = 3;
        let y_move = 1;
        aoc_day3_general_iter(map_block, x_move, y_move)
    }

    pub fn aoc_day3_2(map_block: &[u8]) -> i64 {
        let moves: Vec<(usize, usize)> = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        moves
            .iter()
            .map(|m| aoc_day3_general_iter(map_block, m.0, m.1))
            .product()
    }
}

#[cfg(test)]
mod aoc_tests {
    use super::aoc::*;

    #[test]
    fn test_day1_1() {
        let numbers = vec![1721, 979, 366, 299, 675, 1456];
        assert_eq!(aoc_day1_1(numbers), Some(514_579));
    }

    #[test]
    fn test_day1_2() {
        let numbers = vec![1721, 979, 366, 299, 675, 1456];
        assert_eq!(aoc_day1_2(numbers), Some(241_861_950));
    }

    #[test]
    fn test_day2_1() {
        let passwords = vec!["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"];
        assert_eq!(aoc_day2_1(passwords), 2);
    }

    #[test]
    fn test_day2_2() {
        let passwords = vec!["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"];
        assert_eq!(aoc_day2_2(passwords), 1);
    }

    /// Fixture for day 3 tests
    fn day_3_map<'a>() -> &'a [u8] {
        b"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
"
    }

    #[test]
    fn test_day3_1() {
        let map = day_3_map();
        assert_eq!(aoc_day3_1(map), 7);
    }

    #[test]
    fn test_day3_2() {
        let map = day_3_map();
        assert_eq!(aoc_day3_2(map), 336);
    }

    #[test]
    fn test_day3_2_grid() {
        let map = day_3_map();
        let moves: Vec<(usize, usize)> = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        for m in moves {
            eprintln!("TEST: {} {}", m.0, m.1);
            assert_eq!(
                aoc_day3_general_iter(map, m.0, m.1),
                aoc_day3_general_grid(map, m.0, m.1)
            );
        }
    }
}
